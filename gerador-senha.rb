#!/usr/bin/env ruby -w

def run!

  arquivo = 'dicionario-pt.txt'
  palavras = []

  File.foreach(arquivo) do |linha|

    if linha.length >= 5
      palavras << linha.strip.capitalize
    end

  end

  gerador = GeradorXkcd.new(palavras)
  puts gerador.obter_senha
end


class GeradorXkcd

  attr_reader :adicionar_numeros, :tamanho

  def initialize(palavras, tamanho: 5, adicionar_numeros: true)
    @palavras = palavras
    @tamanho = tamanho
    @adicionar_numeros = adicionar_numeros
  end


  def obter_senha
    (palavras + digito).shuffle.join(" ")
  end

  def palavras
    @palavras.sample(tamanho)
  end

  def digito
    if adicionar_numeros
      [rand(10)]
    else
      [10]
    end
  end

end

run! if __FILE__ == $PROGRAM_NAME
